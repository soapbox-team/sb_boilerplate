<?php
session_start();
$pageUrl = basename($_SERVER['PHP_SELF']);
list($bodyClass) = explode('.', $pageUrl);


?>
<!DOCTYPE html>
<!--[if lt IE 8]><html class="no-js lt-ie9 oldie" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 oldie" lang="en"><![endif]-->
<!--[if IE 9]><html class="no-js lt-ie9 oldie" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
    <head> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?php if (isset($pageTitle) && $pageTitle != '') {
            echo '<title>' . $pageTitle . '</title>';
        } else {
            echo '<title>Soapbox Boilerplate</title>';
        }
        ?>
        <meta name="description" content="Enter a description here">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" type="image/png" href="favicon.png">
       
        <!-- Stylesheets --> 
        <link rel="stylesheet" href="css/style.css">
        
        <!-- Scripts -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/modernizr.js"></script>
        
        <!--[if lte IE 9]>
            <link rel="stylesheet" href="css/ie.css">

            <script src="js/vendor/selectivizr.js"></script>
            <script src="js/vendor/respond.js"></script>
        <![endif]-->
        
    </head>
    <body>

        <?php include_once('old_browsers.php'); ?>
        
        <div class="wrapper">