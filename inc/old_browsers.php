<!--[if lte IE 9]>
	<div id="old-browsers" class="section">
		<div class="row">
			<div class="column large-3">
				<p><strong>PLEASE UPDATE YOUR WEB BROWSER</strong></p>
			</div>
			<div class="column large-9">
				<div class="row">
					<div class="column large-12">
						<p>This website is currently using newer technologies to enhance the user experience across desktop, tablet and smartphone devices. Unfortunately your browser is no longer supported by Microsoft and so we recommend updating to a browser which auto updates and which will keep you safe online. To learn more about web browsers please click here: <a href="http://whatbrowser.org">whatbrowser.org</a></p>
					</div>
				</div>
				<div class="row">
					<div class="column large-12">
						<ul class="medium-block-grid-4">
							<li><img src="http://media.soapbox-creative.co.uk/boilerplate/icon-old-1.png" alt=""><p>Security - Newer browsers protect you from threats online.</p></li>
							<li><img src="http://media.soapbox-creative.co.uk/boilerplate/icon-old-2.png" alt=""><p>Speed - Every new browser generation improves speed</p></li>
							<li><img src="http://media.soapbox-creative.co.uk/boilerplate/icon-old-3.png" alt=""><p>Compatibility - Websites using newer technology will be displayed correctly</p></li>
							<li><img src="http://media.soapbox-creative.co.uk/boilerplate/icon-old-4.png" alt=""><p>User friendly - Giving you a better experience online</p></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
<![endif]-->