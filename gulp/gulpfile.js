"use strict";
/**
 * Declare Gulp Dependancies
 * npm install gulp gulp-sass ttf2woff2 gulp-sourcemaps gulp-watch gulp-autoprefixer gulp-postcss gulp-fontgen gulp-imagemin imagemin-pngquant gulp-changed gulp-concat gulp-uglify gulp-rename es6-promise --save-dev
 */
var gulp          = require('gulp'),
    sass          = require('gulp-sass'),
    maps          = require('gulp-sourcemaps'),
    watch         = require('gulp-watch'),
    autoprefixer  = require('gulp-autoprefixer'),
    postcss       = require('gulp-postcss'),
    fontgen       = require('gulp-fontgen'),
    imagemin      = require('gulp-imagemin'),
    pngquant      = require('imagemin-pngquant'),
    changed       = require('gulp-changed'),
    concat        = require('gulp-concat'),
    uglify       = require('gulp-uglify'),
    rename       = require('gulp-rename');

    require('es6-promise').polyfill();

/**
 * Individual Tasks for Gulp
 * Each can be run from CLI by calling first parameter
 * E.g. gulp watch
 */
gulp.task("watch", function(){
  gulp.watch('./sass/**/*.scss', ['compileSass']);
  gulp.watch('./js/**/*.js', ['buildScript']);
});

gulp.task('compileSass', function() {
  return gulp.src("./sass/style.scss")
    .pipe(maps.init())
    .pipe(sass({
      outputStyle: 'expanded',
    })
    .on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(maps.write('./'))
    .pipe(gulp.dest('../css'));
});

/* Build scripts */
gulp.task("concatVendorScripts", function() {
  return gulp.src('./js/vendor/*')
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('./js/compiled'));
});

gulp.task("concatModulesScripts", function() {
  return gulp.src('./js/modules/*')
    .pipe(concat('modules.js'))
    .pipe(gulp.dest('./js/compiled'));
});

gulp.task("customScript", function() {
  return gulp.src('./js/custom.js')
    .pipe(gulp.dest('./js/compiled'));
});

/* Dev scripts */
gulp.task("buildScript", ['concatVendorScripts', 'concatModulesScripts', 'customScript'], function() {
  return gulp.src(['./js/compiled/vendor.js', './js/compiled/modules.js', './js/compiled/custom.js'])
    .pipe(gulp.dest("../js"));
});

/* Dist scripts */
gulp.task("minifyScripts", ['concatVendorScripts', 'concatModulesScripts', 'customScript'], function() {
  return gulp.src(['compiled/*.js', '!compiled/*.min.js'])
    .pipe(uglify())
    .pipe(rename(function (path) {
      path.extname = ".min.js";
      return path;
    }))
    .pipe(gulp.dest("../js"));
});

/* Builds web fonts from .ttf or .otf */
gulp.task('fontgen', function() {
  return gulp.src("./fonts/*.{ttf,otf}")
    .pipe(fontgen({
      dest: '../fonts'
    }));
});

/* Optimises images */
gulp.task('images', function () {
  return gulp.src(['src_img/**'], {cwd: '.'})
    .pipe(changed('img'))
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
    }))
    .pipe(gulp.dest('../img'));
});

/* SVG 2 PNG */
gulp.task('svg2png', function() {
  gulp.src('./svgs/**/*.svg')
    .pipe(svg2png([1,1]))
    .pipe(gulp.dest('../img/svg'));
});